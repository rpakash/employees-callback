const fs = require("fs");
const path = require("path");

function retriveData(ids) {
  fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
    if (err) {
      throw new Error(err);
    } else {
      let final = JSON.parse(data)["employees"].filter((employe) => {
        return ids.includes(employe.id);
      });

      fs.writeFile(path.join(__dirname, "employeesId.json"), JSON.stringify(final), (err) => {
        if (err) {
          throw new Error(err);
        }
      });
    }
  });
}

function groupBasedOnCompanies(callback) {
  fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
    if (err) {
      throw new Error(err);
    } else {
      let emplyoeesData = JSON.parse(data)["employees"];

      let groupedData = emplyoeesData.reduce((acc, currentPerson) => {
        if (acc[currentPerson.company] === undefined) {
          acc[currentPerson.company] = [];
        }

        acc[currentPerson.company].push(currentPerson);

        return acc;
      }, {});

      fs.writeFile(path.join(__dirname, "groupIntoCompany.json"), JSON.stringify(groupedData), (err) => {
        if (err) {
          throw new Error(err);
        } else if (typeof callback === "function") {
          callback(err, groupedData);
        }
      });
    }
  });
}

function getCompanyDetails(company) {
  groupBasedOnCompanies((err, data) => {
    if (err) {
      throw new Error(err);
    } else {
      let companyData = data[company];
      console.log(companyData);
      fs.writeFile(path.join(__dirname, "dataOfCompany.json"), JSON.stringify(companyData), (err) => {
        if (err) {
          throw new Error(err);
        }
      });
    }
  });
}

function removeEmployee(id) {
  fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
    if (err) {
      throw new Error(err);
    } else {
      let emplyoeesData = JSON.parse(data)["employees"];

      let filteredData = emplyoeesData.filter((person) => {
        return person.id !== id;
      });

      fs.writeFile(path.join(__dirname, "filteredEmployees.json"), JSON.stringify(filteredData), (err) => {
        if (err) {
          throw new Error(err);
        }
      });
    }
  });
}

function sortCompany() {
  fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
    if (err) {
      throw new Error(err);
    } else {
      let employeesData = JSON.parse(data)["employees"];

      employeesData.sort((personA, personB) => {
        if (personA.company === personB.company) {
          return personA.id - personB.id;
        } else {
          return personA.company.localeCompare(personB.company);
        }
      });

      fs.writeFile(path.join(__dirname, "sortedData.json"), JSON.stringify(employeesData), (err) => {
        if (err) {
          throw new Error(err);
        }
      });
    }
  });
}

function swapPosition() {
  fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
    if (err) {
      throw new Error(err);
    } else {
      let parsedData = JSON.parse(data)["employees"];

      let ids = parsedData.reduce((acc, person, index) => {
        if (person.id === 93 || person.id === 92) {
          acc.push(index);
        }
        return acc;
      }, []);

      let tempvar = parsedData[ids[0]];
      parsedData[ids[0]] = parsedData[ids[1]];
      parsedData[ids[1]] = tempvar;

      fs.writeFile(path.join(__dirname, "swaped.json"), JSON.stringify(parsedData), (err) => {
        if (err) {
          throw new Error(err);
        }
      });
    }
  });
}

function addBirthday() {
  fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
    if (err) {
      throw new Error(err);
    } else {
      let parsedData = JSON.parse(data)["employees"];
      let birthday = parsedData.map((person) => {
        if (person.id % 2 === 0) {
          person.birthDay = new Date().toDateString();
        }
        return person;
      });

      fs.writeFile(path.join(__dirname, "birthday.json"), JSON.stringify(birthday), (err) => {
        if (err) {
          throw new Error(err);
        }
      });
    }
  });
}
